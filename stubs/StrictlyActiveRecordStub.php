<?php

namespace Pantagruel74\Yii2StrictlyStubs;

use Pantagruel74\Yii2FileActiveRecord\FileActiveRecord;
use Pantagruel74\Yii2Strictly\StrictlyActiveRecordTrait;

/**
 * @property int $id
 * @property string $val
 */
class StrictlyActiveRecordStub extends FileActiveRecord
{
    use StrictlyActiveRecordTrait;

    /**
     * @return array|string[]
     */
    public function attributes(): array
    {
        return ['id', 'val'];
    }
}
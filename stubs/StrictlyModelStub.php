<?php

namespace Pantagruel74\Yii2StrictlyStubs;

use Pantagruel74\Yii2Strictly\StrictlyModelTrait;
use yii\base\Model;

class StrictlyModelStub extends Model
{
    const _uuid = 'uuid';
    const _count = 'count';

    use StrictlyModelTrait;

    public string $uuid;
    public int $count;

    /**
     * @return array[]
     */
    public function rules(): array
    {
        return [
            [['uuid', 'count'], 'required'],
            [['uuid', 'count'], 'safe'],
        ];
    }
}
<?php

namespace Pantagruel74\Yii2Strictly;

interface UploadFileStrictlyInterface
{
    /**
     * @param string $filePropName
     * @return void
     */
    public function uploadFile(string $filePropName): void;

    /**
     * @param string $filePropName
     * @return void
     */
    public function uploadFileStrictly(string $filePropName): void;

    /**
     * @param string $filePropName
     * @return void
     */
    public function uploadManyFiles(string $filePropName): void;

    /**
     * @param string $filePropName
     * @return void
     */
    public function uploadManyFilesStrictly(string $filePropName): void;

}
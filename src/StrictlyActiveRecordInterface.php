<?php

namespace Pantagruel74\Yii2Strictly;

interface StrictlyActiveRecordInterface
{
    /**
     * @return $this
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function deleteStrictly(): self;

    /**
     * @param bool $runValudation
     * @param string[]|null $attributeNames
     * @return $this
     */
    public function saveStrictly(bool $runValudation = true, ?array $attributeNames = null): self;
}
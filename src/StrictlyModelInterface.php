<?php

namespace Pantagruel74\Yii2Strictly;

interface StrictlyModelInterface
{
    /**
     * @return void
     */
    public function validateStrictly(): void;

    /**
     * @param array $params
     * @param string|null $formName
     * @return void
     */
    /* @phpstan-ignore-next-line */
    public function loadStrictly(array $params, ?string $formName = null): void;

    /**
     * @param array $params
     * @param string|null $formName
     * @return void
     */
    /* @phpstan-ignore-next-line */
    public function loadAndValidateStrictly(array $params, ?string $formName = null): void;

    /**
     * @param array $params
     * @return static
     */
    /* @phpstan-ignore-next-line */
    public static function createStrictlyFromParams(array $params): self;

    /**
     * @param array $params
     * @param string|null $formName
     * @return static
     */
    /* @phpstan-ignore-next-line */
    public static function createStrictlyFromLoad(array $params, ?string $formName = null): self;
}
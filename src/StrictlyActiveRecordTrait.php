<?php

namespace Pantagruel74\Yii2Strictly;

use Webmozart\Assert\Assert;
use yii\db\BaseActiveRecord;

trait StrictlyActiveRecordTrait
{
    /**
     * @return $this
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function deleteStrictly(): self
    {
        /* @var BaseActiveRecord $this */
        $deleteResult = $this->delete();
        Assert::notFalse($deleteResult, self::strictlyDeleteMsg()
            . implode(", ", $this->getFirstErrors()));
        return $this;
    }

    /**
     * @param bool $runValudation
     * @param string[]|null $attributeNames
     * @return $this
     */
    public function saveStrictly(bool $runValudation = true, ?array $attributeNames = null): self
    {
        /* @var BaseActiveRecord $this */
        $saveResult = $this->save($runValudation, $attributeNames);
        Assert::notFalse($saveResult, self::strictlySaveMsg()
            . implode(", ", $this->getFirstErrors()));
        return $this;
    }

    public static function strictlyDeleteMsg(): string
    {
        return static::class . ' delete error: ';
    }

    public static function strictlySaveMsg(): string
    {
        return static::class . ' save error: ';
    }
}
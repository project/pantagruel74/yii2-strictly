<?php

namespace Pantagruel74\Yii2Strictly;

use Webmozart\Assert\Assert;
use yii\base\Model;

trait StrictlyModelTrait
{
    /**
     * @return void
     */
    public function validateStrictly(): void
    {
        /* @var Model $this */
        $result = $this->validate();
        Assert::true($result, self::strictlyValidationMsg()
            . implode(', ', $this->getFirstErrors()));
    }

    /**
     * @param array $params
     * @param string|null $formName
     * @return void
     */
    public function loadStrictly(array $params, ?string $formName = null): void
    {
        /* @var Model $this */
        $result = $this->load($params, $formName);
        Assert::true($result, self::strictlyLoadingMsg()
            . implode(', ', $this->getFirstErrors()));
    }

    /**
     * @param array $params
     * @param string|null $formName
     * @return void
     */
    public function loadAndValidateStrictly(array $params, ?string $formName = null): void
    {
        $this->loadStrictly($params, $formName);
        $this->validateStrictly();
    }

    /**
     * @param array $params
     * @return static
     */
    public static function createStrictlyFromParams(array $params): self
    {
        $model = new self($params);
        $model->validateStrictly();
        return $model;
    }

    /**
     * @param array $params
     * @param string|null $formName
     * @return static
     */
    public static function createStrictlyFromLoad(array $params, ?string $formName = null): self
    {
        $model = new self();
        $model->loadAndValidateStrictly($params, $formName);
        return $model;
    }

    public static function strictlyValidationMsg(): string
    {
        return "Model validation error: ";
    }

    public static function strictlyLoadingMsg(): string
    {
        return "Model loading error: ";
    }

}
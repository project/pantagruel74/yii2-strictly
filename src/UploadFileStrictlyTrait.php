<?php

namespace Pantagruel74\Yii2Strictly;

use Webmozart\Assert\Assert;
use yii\base\Model;
use yii\web\UploadedFile;

trait UploadFileStrictlyTrait
{
    public function uploadFile(string $filePropName): void
    {
        /* @var Model $this */
        $this->$filePropName = UploadedFile::getInstance($this, $filePropName);
    }

    public function uploadFileStrictly(string $filePropName): void
    {
        $this->uploadFile($filePropName);
        Assert::notNull($this->$filePropName);
    }

    public function uploadManyFiles(string $filePropName): void
    {
        /* @var Model $this */
        $this->$filePropName = UploadedFile::getInstances($this, $filePropName);
    }

    public function uploadManyFilesStrictly(string $filePropName): void
    {
        $this->uploadManyFiles($filePropName);
        Assert::notNull($this->$filePropName);
    }
}
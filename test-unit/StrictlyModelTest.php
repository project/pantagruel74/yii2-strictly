<?php

namespace Pantagruel74\Yii2StrictlyTestUnit;

use Pantagruel74\Yii2Loader\Yii2Loader;
use Pantagruel74\Yii2StrictlyStubs\StrictlyModelStub;
use PHPUnit\Framework\TestCase;

class StrictlyModelTest extends TestCase
{
    /**
     * @param string|null $name
     * @param array $data
     * @param $dataName
     */
    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        Yii2Loader::load();
        parent::__construct($name, $data, $dataName);
    }

    /**
     * @return void
     */
    public function testCorrectValidation(): void
    {
        $model = new StrictlyModelStub([
            StrictlyModelStub::_uuid => 'xc21c4124c',
            StrictlyModelStub::_count => 12,
        ]);
        $model->validateStrictly();
        $this->assertEquals([], $model->getFirstErrors());
    }

    /**
     * @return void
     */
    public function testIncorrectValidation(): void
    {
        $model = new StrictlyModelStub([
            StrictlyModelStub::_uuid => '',
            StrictlyModelStub::_count => 0,
        ]);
        $this->expectException(\InvalidArgumentException::class);
        $model->validateStrictly();
    }

    /**
     * @return void
     * @throws \yii\base\InvalidConfigException
     */
    public function testCorrectLoading(): void
    {
        $model = new StrictlyModelStub();
        $loadingData = [
            $model->formName() => [
                StrictlyModelStub::_uuid => 'xc21c4124c',
                StrictlyModelStub::_count => 12,
            ],
        ];
        $model->loadAndValidateStrictly($loadingData);
        $this->assertEquals('xc21c4124c', $model->uuid);
        $this->assertEquals(12, $model->count);
        $this->assertEquals([], $model->getFirstErrors());
    }

    /**
     * @return void
     * @throws \yii\base\InvalidConfigException
     */
    public function testIncorrectLoading(): void
    {
        $model = new StrictlyModelStub();
        $loadingData = [
            $model->formName() . '!' => [
                'aboba' => 'xc21c4124c',
            ],
        ];
        $this->expectException(\InvalidArgumentException::class);
        $model->loadStrictly($loadingData);
    }

    /**
     * @return void
     */
    public function testCreatingFromLoad(): void
    {
        $loadingData = [
            'myForm' => [
                StrictlyModelStub::_uuid => 'xc21c4124c',
                StrictlyModelStub::_count => 12,
            ],
        ];
        $model = StrictlyModelStub::createStrictlyFromLoad($loadingData, 'myForm');
        $this->assertEquals('xc21c4124c', $model->uuid);
        $this->assertEquals(12, $model->count);
        $this->assertEquals([], $model->getFirstErrors());
    }
}
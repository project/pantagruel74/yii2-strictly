<?php

namespace Pantagruel74\Yii2StrictlyTestUnit;

use Pantagruel74\Yii2StrictlyStubs\StrictlyActiveRecordStub;
use Pantagruel74\Yii2TestAppTestHelpers\AbstractBaseTest;

class StrictlyActiveRecordTest extends AbstractBaseTest
{
    /**
     * @return array
     */
    protected function getConfig(): array
    {
        return include __DIR__ . DIRECTORY_SEPARATOR . 'config.php';
    }

    /**
     * @return void
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    protected function testScenario(): void
    {
        $this->removeAllRecords();
        $data = [
            'id' => 1,
            'val' => 'some-val'
        ];
        $this->saveNewRecordWithData($data);
        $this->assertHasRecordWithData($data);
        $this->deleteStrictly();
        $this->assertCount(0, StrictlyActiveRecordStub::find()->all());
    }

    /**
     * @return void
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    protected function removeAllRecords(): void
    {
        foreach (StrictlyActiveRecordStub::find()->all() as $rec)
        {
            /* @var StrictlyActiveRecordStub $rec */
            $rec->delete();
        }
    }

    /**
     * @return void
     * @throws \yii\db\Exception
     * @throws \yii\db\StaleObjectException
     */
    protected function deleteStrictly(): void
    {
        $recs = StrictlyActiveRecordStub::find()->all();
        $this->assertCount(1, $recs);
        $r = current($recs);
        /* @var StrictlyActiveRecordStub $r */
        $r->deleteStrictly();
    }

    /**
     * @param array $data
     * @return void
     */
    protected function saveNewRecordWithData(array $data): void
    {
        $rec = new StrictlyActiveRecordStub($data);
        $rec->saveStrictly();
    }

    /**
     * @param array $data
     * @return void
     */
    protected function assertHasRecordWithData(array $data): void
    {
        $recs = StrictlyActiveRecordStub::find()->all();
        $this->assertCount(1, $recs);
        $r = current($recs);
        $this->assertEquals($data, $r->getAttributes());
    }
}
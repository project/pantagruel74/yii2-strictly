<?php
$config = \Pantagruel74\Yii2TestApp\YiiDefaultConfig::getBase(__DIR__);
$config['components']['filedb'] = [
    'class' => \yii2tech\filedb\Connection::class,
    'path' => __DIR__ . DIRECTORY_SEPARATOR . 'filedb',
];
return $config;